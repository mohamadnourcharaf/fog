//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#include "Host.h"

Host::Host(std::string title, double processor, double memory, double disk, double time, double distance){

    this->title = title;
    this->processor = processor;
    this->memory = memory;
    this->disk = disk;
    this->time = time;
    this->distance = distance;
}

Host::Host(Host *oldHost){
    this->title = oldHost->title;
    this->processor = oldHost->processor;
    this->memory = oldHost->memory;
    this->disk = oldHost->disk;
    this->time = oldHost->time;
    this->distance = oldHost->distance;
}
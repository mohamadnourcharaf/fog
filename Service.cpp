//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#include "Service.h"

Service::Service(std::string title, double processor, double memory, double disk, double priority){

    this->title = title;
    this->processor = processor;
    this->memory = memory;
    this->disk = disk;
    this->priority = priority;
}
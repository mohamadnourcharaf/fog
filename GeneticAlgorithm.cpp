//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#include "GeneticAlgorithm.h"

unsigned elitePopulationSize = unsigned(elitePercentage * populationSize);
unsigned offspringPopulationSize = populationSize - elitePopulationSize;
unsigned survivingPopulationSize = unsigned(survivingPercentage * populationSize);

GeneticAlgorithm::GeneticAlgorithm(std::vector<Service *> *services, std::vector<Host *> *hosts){

    this->services = services;
    this->hosts = hosts;
}

// Methods
void GeneticAlgorithm::start(){

    // Log Paper Chromosome
    logPaperChromosome();

    // Initialize Population
    initializePopulation();

    // Repair Population
    repairChromosomes(chromosomes);

    // Fitness: Calculate Population Fitness Values and Probabilities
    calculatePopulationFitnessValues();

    // Sort: Sort Population based on Fitness Value
    sortPopulation();

    // Probabilities: Calculate Population Probabilities
    calculatePopulationProbabilities();

    // Process Generations
    processGenerations();
}

void GeneticAlgorithm::logPaperChromosome(){
    std::cout << "Paper Chromosome:" << std::endl;
    Chromosome *paperChromosome = new Chromosome(services,hosts);
    paperChromosome->initializeK();
    if (scenario == 2){
        paperChromosome->K->at(0)->at(3)->value = 1;
        paperChromosome->K->at(1)->at(1)->value = 1;
        paperChromosome->K->at(2)->at(3)->value = 1;
        paperChromosome->K->at(3)->at(2)->value = 1;
        paperChromosome->K->at(4)->at(0)->value = 1;
        paperChromosome->K->at(5)->at(3)->value = 1;
    }
    else{
        paperChromosome->K->at(0)->at(2)->value = 1;
        paperChromosome->K->at(1)->at(3)->value = 1;
        paperChromosome->K->at(2)->at(1)->value = 1;
    }
    paperChromosome->repair();
    paperChromosome->calculateFitness();
    logResults(-1,paperChromosome);
}

void GeneticAlgorithm::initializePopulation(){

    bestChromosomes = new std::vector<Chromosome *>();

    chromosomes = new std::vector<Chromosome *>();

    for (unsigned i = 0; i < populationSize; ++i){
        Chromosome *chromosome = new Chromosome(services,hosts);
        chromosomes->push_back(chromosome);

        chromosome->initializeK();
        chromosome->randomlyAssignServicesToHosts();
    }
}

void GeneticAlgorithm::repairChromosomes(std::vector<Chromosome *> *chromosomesToRepair){
    for (Chromosome *chromosome : *chromosomesToRepair){
        chromosome->repair();
    }
}

void GeneticAlgorithm::processGenerations(){

    for (unsigned i = 0; i < numberOfGenerations; ++i){
        processGeneration(i);
    }
}

void GeneticAlgorithm::processGeneration(unsigned generationNumber){

    // Elitism: Select Elite Population
    std::vector<Chromosome *> *eliteChromosomes = selectElitePopulation();

    // Selection: Select Surviving Population: Roulette Wheel Selection
    std::vector<Chromosome *> *survivingChromosomes = selectSurvivingPopulation();

    // Crossover: Crossover Surviving Population using Uniform Crossover to Generate Offspring Population
    std::vector<Chromosome *> *offspringChromosomes = crossover(survivingChromosomes);

    // Mutatation: Mutate Offspring Chromosomes
    mutation(offspringChromosomes);

    // Replace: Replace existing population with elite population and offspring population
    chromosomes->clear();
    chromosomes->insert(chromosomes->end(), eliteChromosomes->begin(), eliteChromosomes->end());
    chromosomes->insert(chromosomes->end(), offspringChromosomes->begin(), offspringChromosomes->end());

    // Repair Chromosomes
    repairChromosomes(chromosomes);

    // Fitness: Calculate Population Fitness Values and Probabilities
    calculatePopulationFitnessValues();

    // Sort: Sort Population based on Fitness Value
    sortPopulation();

    // Probabilities: Calculate Population Probabilities
    calculatePopulationProbabilities();

    // Keep track of Best Chromosome
    Chromosome *bestChromosome = chromosomes->at(0);
    bestChromosomes->push_back(bestChromosome);

    // Log Results of Best Chromosome
    logResults(generationNumber, bestChromosome);

    if (bestChromosomes->size() > 1 && bestChromosomes->at(bestChromosomes->size()-1)->fitnessValue > bestChromosomes->at(bestChromosomes->size()-2)->fitnessValue){
        std::cout << "Solution is improving" << std::endl;
        std::cout << "End" << std::endl;
    }
}

void GeneticAlgorithm::calculatePopulationFitnessValues(){

    // Calculate F1 to F5
    for (Chromosome *chromosome : *chromosomes){
        chromosome->calculateFitness();
    }
}

void GeneticAlgorithm::sortPopulation(){
    // Sort Population
    std::sort(chromosomes->begin(), chromosomes->end(),[](Chromosome *a, Chromosome *b) -> bool{
        return a->fitnessValue > b->fitnessValue;
    });
}

void GeneticAlgorithm::calculatePopulationProbabilities(){
    // Probabilities
    double sumOfFitnessValues = 0;
    for (unsigned i = 0; i < chromosomes->size(); ++i){
        sumOfFitnessValues += chromosomes->at(i)->fitnessValue;
    }

    double nextProbabilityWheelStart = 0;
    for (unsigned i = 0; i < chromosomes->size(); ++i){
        chromosomes->at(i)->probability = chromosomes->at(i)->fitnessValue/sumOfFitnessValues;
        chromosomes->at(i)->probabilityWheelStart = nextProbabilityWheelStart;
        chromosomes->at(i)->probabilityWheelEnd = chromosomes->at(i)->probabilityWheelStart + chromosomes->at(i)->probability;
        nextProbabilityWheelStart = chromosomes->at(i)->probabilityWheelEnd;
    }
}

std::vector<Chromosome *> * GeneticAlgorithm::selectElitePopulation(){

    std::vector<Chromosome *> *eliteChromosomes = new std::vector<Chromosome *>();

    for (unsigned i = 0; i < elitePopulationSize; ++i){
        Chromosome *newChromosome = new Chromosome(chromosomes->at(i));
        eliteChromosomes->push_back(newChromosome);
    }

    return eliteChromosomes;
}

std::vector<Chromosome *> * GeneticAlgorithm::selectSurvivingPopulation(){

    std::vector<Chromosome *> *survivingChromosomes = new std::vector<Chromosome *>();

    std::vector<Chromosome *> *potentialChromosomes = new std::vector<Chromosome *>();

    potentialChromosomes->insert(potentialChromosomes->end(),chromosomes->begin() + elitePopulationSize,chromosomes->end());

    std::random_device rd;
    std::mt19937 e2(rd());

    for (unsigned i = 0; i < survivingPopulationSize; ++i){

        Chromosome *winnerChromosome = nullptr;

        // Select a chromosome randomly only once
        while (true){
            std::uniform_real_distribution<> distribution(potentialChromosomes->front()->probabilityWheelStart, potentialChromosomes->back()->probabilityWheelEnd);
            double random = distribution(e2);

            for (unsigned j = 0; j < potentialChromosomes->size(); ++j){

                Chromosome *potentialChromosome = potentialChromosomes->at(j);
                if ((random >= potentialChromosome->probabilityWheelStart && random <= potentialChromosome->probabilityWheelEnd && !potentialChromosome->isSelected)){
                    winnerChromosome = potentialChromosome;
                    potentialChromosome->isSelected = true;
                    break;
                }
            }

            if (winnerChromosome != nullptr){
                // A chromosome was selected
                break;
            }

            // Increase probabilities by percentage
            double nextProbabilityWheelStart = 0;
            for (unsigned j = 0; j < potentialChromosomes->size(); ++j){
                double newProbability = potentialChromosomes->at(j)->probability + probabilityIncrement;
                potentialChromosomes->at(j)->probability = newProbability;
                potentialChromosomes->at(j)->probabilityWheelStart = nextProbabilityWheelStart;
                potentialChromosomes->at(j)->probabilityWheelEnd = potentialChromosomes->at(j)->probabilityWheelStart + potentialChromosomes->at(j)->probability;
                nextProbabilityWheelStart = potentialChromosomes->at(j)->probabilityWheelEnd;
            }
        }

        Chromosome *newChromosome = new Chromosome(winnerChromosome);
        survivingChromosomes->push_back(newChromosome);
    }

    return survivingChromosomes;
}

std::vector<Chromosome *> * GeneticAlgorithm::crossover(std::vector<Chromosome *> *survivingChromosomes){

    Utilities::random_selector<> selector{};

    std::random_device rd;
    std::mt19937 e2(rd());
    std::uniform_real_distribution<> distribution(0, 1);

    std::vector<Chromosome *> *offspringChromosomes = new std::vector<Chromosome *>();

    while (true){
        // Pick parent 1 randomly
        Chromosome *parent1 = *selector(survivingChromosomes->begin(),survivingChromosomes->end());

        // Pick parent 2 randomly, such that it is different from parent 1
        Chromosome *parent2;
        bool KsAreTheSame = false;
        for (unsigned i = 0; i < crossoverTries; ++i){
            parent2 = *selector(survivingChromosomes->begin(),survivingChromosomes->end());

            KsAreTheSame = parent2->compareK(parent1);
            if (!KsAreTheSame){
                break;
            }
        }

        if (KsAreTheSame){
            parent2->initializeK();
            parent2->randomlyAssignServicesToHosts();
        }

        // Split Vertically at random index
        double random = distribution(e2);
        double cutPointPercent = 0.5; // Note this percent can be set to random
        unsigned cutColumn = (unsigned)((unsigned)hosts->size()*cutPointPercent);

        for (unsigned i = 0; i < services->size(); ++i){

            std::vector<unsigned > *parent1HostIndecies = new std::vector<unsigned >;
            std::vector<unsigned > *parent2HostIndecies = new std::vector<unsigned >;

            for (unsigned j = 0; j < hosts->size(); ++j) {
                if (j <= cutColumn){
                    parent1->K->at(i)->at(j)->value = parent2->K->at(i)->at(j)->value;
                }
                else{
                    parent2->K->at(i)->at(j)->value = parent1->K->at(i)->at(j)->value;
                }

                if (parent1->K->at(i)->at(j)->value == 1){
                    parent1HostIndecies->push_back(j);
                }
                if (parent2->K->at(i)->at(j)->value == 1){
                    parent2HostIndecies->push_back(j);
                }
            }

            // Repair: Check if each service in parent 1 and 2 are pushed to more than one host
            // If true, then select onle one host to push to randomly
            if (parent1HostIndecies->size() >= 2){
                unsigned hostIndex = *selector(parent1HostIndecies->begin(),parent1HostIndecies->end());
                for (unsigned j = 0; j < hosts->size(); ++j){
                    parent1->K->at(i)->at(j)->value = 0;
                }
                parent1->K->at(i)->at(hostIndex)->value = 1;
            }
            if (parent2HostIndecies->size() >= 2){
                unsigned hostIndex = *selector(parent2HostIndecies->begin(),parent2HostIndecies->end());
                for (unsigned j = 0; j < hosts->size(); ++j){
                    parent2->K->at(i)->at(j)->value = 0;
                }
                parent2->K->at(i)->at(hostIndex)->value = 1;
            }
        }

        if (offspringChromosomes->size() != offspringPopulationSize){

            Chromosome *offspring1 = new Chromosome(parent1);
            offspringChromosomes->push_back(offspring1);
        }
        else{
            break;
        }

        if (offspringChromosomes->size() != offspringPopulationSize){

            Chromosome *offspring2 = new Chromosome(parent2);
            offspringChromosomes->push_back(offspring2);
        }
        else{
            break;
        }
    }

    return offspringChromosomes;
}

void GeneticAlgorithm::mutation(std::vector<Chromosome *> *offspringChromosomes){

    Utilities::random_selector<> selector{};

    std::random_device rd;
    std::mt19937 e2(rd());
    std::uniform_real_distribution<> distribution(0, 1);

    for (unsigned k = 0; k < offspringChromosomes->size(); ++k){
        for (unsigned i = 0; i < services->size(); ++i){

            double random = distribution(e2);
            if (mutationProbability >= random){

                std::vector<Gene *> *genesRow = chromosomes->at(k)->K->at(i);

                for (unsigned j = 0; j < hosts->size(); ++j){
                    genesRow->at(j)->value = 0;
                }

                Gene *gene = *selector(genesRow->begin(), genesRow->end());
                gene->value = 1;
            }
        }
    }
}

void GeneticAlgorithm::logResults(unsigned generationNumber, Chromosome *chromosome){

    std::cout << "Generation Number: " + std::to_string(generationNumber + 1) << std::endl;

    std::cout <<
    "F1: " + std::to_string(chromosome->f1) + " " +
    "F2: " + std::to_string(chromosome->f2) + " " +
    "F3: " + std::to_string(chromosome->f3) + " " +
    "F4: " + std::to_string(chromosome->f4) + " " +
    "F5: " + std::to_string(chromosome->f5)
    << std::endl;

    std::cout << "Fitness Value: " + std::to_string(chromosome->fitnessValue) << std::endl;

    chromosome->printK();
    std::cout << "****************************************************************************************************" << std::endl;
}
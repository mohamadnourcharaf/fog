//
// Created by Mohamad Nour Charaf on 8/7/20.
//

#ifndef FOG_DATAMANAGER_H
#define FOG_DATAMANAGER_H

#include "Host.h"
#include "Service.h"

class DataManager {

public:

    static std::vector<Service *> * getAllServices();
    static std::vector<Host *> * getAllHosts();
    static std::vector<Service *> * getScenario1Services();
    static std::vector<Host *> * getScenario1Hosts();
    static std::vector<Service *> * getScenario2Services();
    static std::vector<Host *> * getScenario2Hosts();
};


#endif //FOG_DATAMANAGER_H

//
// Created by Mohamad Nour Charaf on 8/7/20.
//

#ifndef FOG_PARAMETERS_H
#define FOG_PARAMETERS_H

#include "cstdio"
#include <string>
#include <vector>
#include <cmath>
#include <random>
#include <iterator>
#include <iostream>
#include <sstream>
#include <algorithm>

// Parameters
static unsigned scenario = 1;
static double W1 = 0.2;
static double W2 = 0.2;
static double W3 = 0.2;
static double W4 = 0.2;
static double W5 = 0.2;
static double C = 10;
static double D = 0;

static unsigned populationSize = 30;
static unsigned numberOfGenerations = 10000;
static double elitePercentage = 0.1;
static double survivingPercentage = 0.4;
static double probabilityIncrement = 0.01;
static double crossoverTries = 20;
static double mutationProbability = scenario == 2 ? (1.0/6) : (1.0/3);

#endif //FOG_PARAMETERS_H

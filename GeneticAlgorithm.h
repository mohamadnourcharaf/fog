//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#ifndef FOG_GENETICALGORITHM_H
#define FOG_GENETICALGORITHM_H

#include "Utilities.h"

#include "Chromosome.h"

class GeneticAlgorithm {

public:

    GeneticAlgorithm(std::vector<Service *> *services, std::vector<Host *> *hosts);

    std::vector<Service *> *services;
    std::vector<Host *> *hosts;
    std::vector<Chromosome *> *chromosomes;
    std::vector<Chromosome *> *bestChromosomes;

    void start();
    void logPaperChromosome();
    void initializePopulation();
    void repairChromosomes(std::vector<Chromosome *> *chromosomesToRepair);
    void processGenerations();
    void processGeneration(unsigned generationNumber);
    void calculatePopulationFitnessValues();
    void sortPopulation();
    void calculatePopulationProbabilities();
    std::vector<Chromosome *> * selectElitePopulation();
    std::vector<Chromosome *> * selectSurvivingPopulation();
    std::vector<Chromosome *> * crossover(std::vector<Chromosome *> *survivingChromosomes);
    void mutation(std::vector<Chromosome *> *offspringChromosomes);
    void logResults(unsigned generationNumber, Chromosome *chromosome);
};


#endif //FOG_GENETICALGORITHM_H

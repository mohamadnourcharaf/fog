//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#include "Chromosome.h"

#include "Utilities.h"

Chromosome::Chromosome(std::vector<Service *> *services, std::vector<Host *> *hosts){

    this->services = services;
    this->hosts = hosts;

    // Hosts Sorted By Time
    hostsSortedByTime = new std::vector<Host *>();
    for (Host *host : *hosts){
        hostsSortedByTime->push_back(new Host(host));
    }

    std::sort(hostsSortedByTime->begin(), hostsSortedByTime->end(),[](Host *a, Host *b) -> bool{
        return a->time > b->time;
    });

    // Hosts Sorted By Distance
    hostsSortedByDistance = new std::vector<Host *>();
    for (Host *host : *hosts){
        hostsSortedByDistance->push_back(new Host(host));
    }

    std::sort(hostsSortedByDistance->begin(), hostsSortedByDistance->end(),[](Host *a, Host *b) -> bool{
        return a->distance > b->distance;
    });
}

Chromosome::Chromosome(Chromosome *oldChromosome) {

    this->services = oldChromosome->services;
    this->hosts = oldChromosome->hosts;
    this->hostsSortedByTime = oldChromosome->hostsSortedByTime;
    this->hostsSortedByDistance = oldChromosome->hostsSortedByDistance;

    this->initializeK();

    for (unsigned i = 0; i < services->size(); ++i) {
        for (unsigned j = 0; j < hosts->size(); ++j){
            this->K->at(i)->at(j)->service = oldChromosome->K->at(i)->at(j)->service;
            this->K->at(i)->at(j)->host = oldChromosome->K->at(i)->at(j)->host;
            this->K->at(i)->at(j)->value = oldChromosome->K->at(i)->at(j)->value;
        }
    }

    this->updateL();
    this->updateR();

    this->f1 = oldChromosome->f1;
    this->f2 = oldChromosome->f2;
    this->f3 = oldChromosome->f3;
    this->f4 = oldChromosome->f4;
    this->f5 = oldChromosome->f5;
    this->fitnessValue = oldChromosome->fitnessValue;
    this->probability = oldChromosome->probability;
    this->probabilityWheelStart = oldChromosome->probabilityWheelStart;
    this->probabilityWheelEnd = oldChromosome->probabilityWheelEnd;
}

// Methods
void Chromosome::initializeK(){

    K = new std::vector<std::vector<Gene *> *>();

    for (unsigned i = 0; i < services->size(); ++i){
        Service *service = services->at(i);
        std::vector<Gene *> *genesRow = new std::vector<Gene *>();
        K->push_back(genesRow);
        for (unsigned j = 0; j < hosts->size(); ++j){
            Host *host = hosts->at(j);
            Gene *gene = new Gene(service, host, 0);
            genesRow->push_back(gene);
        }
    }
}
void Chromosome::printK(){

    std::cout << "K   ";

    for (unsigned j = 0; j < hosts->size(); ++j){
        std::cout << hosts->at(j)->title + " ";
    }

    std::cout << std::endl;

    for (unsigned i = 0; i < services->size(); ++i){

        std::cout << services->at(i)->title + "  ";

        for (unsigned j = 0; j < hosts->size(); ++j){
            std::cout << K->at(i)->at(j)->value << "  ";
        }
        std::cout << std::endl;
    }
}

void Chromosome::updateL(){

    L = new std::vector<unsigned>();

    // Loop in Hosts
    for (unsigned j = 0; j < hosts->size();++j){

        L->push_back(0);

        // Loop in Services
        for (unsigned i = 0; i < services->size(); ++i){
            Gene *gene = K->at(i)->at(j);

            if (gene->value == 1){
                L->at(j) = 1;
                break;
            }
        }
    }
}

void Chromosome::updateR(){

    R = new std::vector<unsigned>();

    // Loop in Services
    for (unsigned i = 0; i < services->size();++i){

        R->push_back(0);

        // Loop in Hosts
        for (unsigned j = 0; j < hosts->size(); ++j){
            Gene *gene = K->at(i)->at(j);

            if (gene->value == 1){
                R->at(i) = 1;
                break;
            }
        }
    }
}

void Chromosome::randomlyAssignServicesToHosts(){

    Utilities::random_selector<> selector{};

    for (unsigned i = 0; i < K->size(); ++i){
        std::vector<Gene *> *genesRow = K->at(i);

        Gene *gene = *selector(genesRow->begin(), genesRow->end());
        gene->value = 1;
    }
}

void Chromosome::repair(){

    // Loop in services
    for (unsigned i = 0; i < services->size(); ++i){

        bool serviceIsPushed = false;
        bool hostIsFeasible;
        unsigned hostIndex;

        for (unsigned j = 0; j < hosts->size(); ++j){
            if (K->at(i)->at(j)->value == 1){
                serviceIsPushed = true;
                hostIndex = j;
                hostIsFeasible = isHostFeasible(j);
                break;
            }
        }

        // Check if service was pushed
        if (!serviceIsPushed){
            continue;
        }

        // Check if host is feasible
        if (hostIsFeasible){
            continue;
        }

        // Attempt 1: Try pushing to another host
        std::vector<unsigned > *hostIndecies = new std::vector<unsigned >();
        for (unsigned j = 0; j < hosts->size(); ++j){
            if (j != hostIndex){
                hostIndecies->push_back(j);
            }
        }
        // Shuffle Host Indecies
        auto rd = std::random_device {};
        auto rng = std::default_random_engine { rd() };
        std::shuffle(hostIndecies->begin(), hostIndecies->end(), rng);

        K->at(i)->at(hostIndex)->value = 0; // Set Value
        for (unsigned j : *hostIndecies){
            K->at(i)->at(j)->value = 1; // Set Value
            hostIsFeasible = isHostFeasible(j);
            if (hostIsFeasible){
                break;
            }
            else{
                K->at(i)->at(j)->value = 0; // Revert Value
            }
        }

        if (hostIsFeasible){
            continue;
        }

        K->at(i)->at(hostIndex)->value = 1; // Revert Value

        // Attempt 2: Don't push this service
        K->at(i)->at(hostIndex)->value = 0;
    }

    updateL();
    updateR();

    // Note that services priority constraint would be met through the genetic algorithm; services with high priority will not be forcefully pushed
}

bool Chromosome::isHostFeasible(unsigned hostIndex){

    bool isFeasible = true;

    // Physical Resources Capacity
    double processorUsage = 0;
    double memoryUsage = 0;
    double diskUsage = 0;

    // Loop in services of host
    for (unsigned i = 0; i < services->size(); ++i){

        if (K->at(i)->at(hostIndex)->value == 1){
            processorUsage += services->at(i)->processor;
            memoryUsage += services->at(i)->memory;
            diskUsage += services->at(i)->disk;
        }

        // Unique Placement of Service
        unsigned numberOfHosts = 0;
        for (unsigned j = 0; j < hosts->size(); ++j){
            numberOfHosts += K->at(i)->at(j)->value;
        }

        if (numberOfHosts > 1){
            isFeasible = false;
            exit(0); // ERROR: This should have been handled in the mutation and crossover operators, something went wrong
            break;
        }
    }

    if (processorUsage > hosts->at(hostIndex)->processor || memoryUsage > hosts->at(hostIndex)->memory || diskUsage > hosts->at(hostIndex)->disk){
        isFeasible = false;
    }

    // Minimum Survivability Time
    if (hosts->at(hostIndex)->time < D){
        isFeasible = false;
    }

    return isFeasible;
}

void Chromosome::calculateFitness(){

    updateL();
    updateR();

    f1 = 0;
    f2 = 0;
    f3 = 0;
    f4 = 0;
    f5 = 0;
    fitnessValue = 0;

    // Loop in Services
    for (unsigned i = 0; i < services->size();++i){
        // Loop in Hosts
        for (unsigned j = 0; j < hosts->size();++j){
            f1 += K->at(i)->at(j)->value;
        }
    }

    // Loop in Services
    for (unsigned i = 0; i < services->size();++i){
        f2 += pow(C,services->at(i)->priority) * services->at(i)->priority * R->at(i);
    }

    // Loop in Hosts
    for (unsigned j = 0; j < hosts->size();++j) {
        f3 += hosts->at(j)->time * L->at(j);
        f4 += hosts->at(j)->distance * L->at(j);
        f5 += L->at(j);
    }

    // Normalization
    // Normalize F1
    double f1Min = 0;
    double f1Max = services->size(); // F1 Max: Assume all services are pushed
    f1 = (f1 - f1Min) / (f1Max - f1Min);

    // Normalize F2
    double f2Min = 0;
    double f2Max = 0; // F2 Max: Assume all services are pushed
    for (Service *service : *services){
        f2Max += pow(C,service->priority) * service->priority * 1;
    }

    f2 = (f2 - f2Min) / (f2Max - f2Min);

    // Number of active hosts
    unsigned numberOfActiveHosts = services->size() < hosts->size() ? services->size() : hosts->size();
    // Normalize F3
    double f3Min = 0;
    double f3Max = 0; // F3 Max: Assume all services are pushed on unique hosts
    for (unsigned j = 0; j < hosts->size(); ++j){
        f3Max += hosts->at(j)->time;
    }

    f3 = (f3 - f3Min) / (f3Max - f3Min);

    // Normalize F4
    double f4Min = 0;
    double f4Max = 0; // F4 Max: Assume all services are pushed on unique hosts
    for (unsigned j = 0; j < hosts->size(); ++j){
        f4Max += hosts->at(j)->distance;
    }

    f4 = (f4 - f4Min) / (f4Max - f4Min);

    // Normalize F5
    double f5Min = 0;
    double f5Max = hosts->size(); // F5 Max: Assume all services are pushed on unique hosts
    f5 = (f5 - f5Min) / (f5Max - f5Min);

    // Fitness Value
    fitnessValue = (f1*W1) + (f2*W2) + (f3*W3) - (f4*W4) - (f5*W5);
}

bool Chromosome::compareK(Chromosome *chromosome) {
    bool KsAreTheSame = true;
    for (unsigned i = 0; i < services->size(); ++i){
        for (unsigned j = 0; j < hosts->size(); ++j){
            if (this->K->at(i)->at(j)->value != chromosome->K->at(i)->at(j)->value){
                KsAreTheSame = false;
                break;
            }
        }
        if (!KsAreTheSame){
            break;
        }
    }

    return KsAreTheSame;
}

//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#include "Parameters.h"
#include "DataManager.h"
#include "GeneticAlgorithm.h"

std::vector<Service *> * getServices();
std::vector<Host *> * getHosts();

int main(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";

    std::vector<Service *> *services = scenario == 2 ? DataManager::getScenario2Services() : DataManager::getScenario1Services();
    std::vector<Host *> *hosts = scenario == 2 ? DataManager::getScenario2Hosts() : DataManager::getScenario1Hosts();

    GeneticAlgorithm *geneticAlgorithm = new GeneticAlgorithm(services,hosts);

    geneticAlgorithm->start();

    return 0;
}

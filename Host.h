//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#ifndef FOG_HOST_H
#define FOG_HOST_H

#include "Service.h"

class Host {

public:
    Host(std::string title,double processor, double memory, double disk, double time, double distance);
    Host(Host *oldHost);

    std::string title;
    double processor;
    double memory;
    double disk;
    double time;
    double distance;
};


#endif //FOG_HOST_H

//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#ifndef FOG_CHROMOSOME_H
#define FOG_CHROMOSOME_H

#include "Parameters.h"

#include "Gene.h"

class Chromosome {

public:

    Chromosome(std::vector<Service *> *services, std::vector<Host *> *hosts);
    Chromosome(Chromosome *oldChromosome);

    std::vector<Service *> *services;
    std::vector<Host *> *hosts;
    std::vector<Host *> *hostsSortedByTime;
    std::vector<Host *> *hostsSortedByDistance;
    std::vector<std::vector<Gene *> *> *K;
    std::vector<unsigned> *L;
    std::vector<unsigned> *R;
    double f1;
    double f2;
    double f3;
    double f4;
    double f5;
    double fitnessValue;
    double probability;
    double probabilityWheelStart;
    double probabilityWheelEnd;

    bool isSelected;

    void initializeK();
    void printK();
    void updateL();
    void updateR();
    void randomlyAssignServicesToHosts();
    void repair();
    bool isHostFeasible(unsigned hostIndex);
    void calculateFitness();
    bool compareK(Chromosome *chromosome);
};


#endif //FOG_CHROMOSOME_H

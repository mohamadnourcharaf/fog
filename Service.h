//
// Created by Mohamad Nour Charaf on 7/24/20.
//

#ifndef FOG_SERVICE_H
#define FOG_SERVICE_H

#include "Parameters.h"

class Service{

public:

    Service(std::string title,double processor, double memory, double disk, double priority);

    std::string title;
    double processor;
    double memory;
    double disk;
    double priority;
};


#endif //FOG_SERVICE_H

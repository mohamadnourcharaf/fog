//
// Created by Mohamad Nour Charaf on 8/7/20.
//

#ifndef FOG_GENE_H
#define FOG_GENE_H

#include "Service.h"
#include "Host.h"

class Gene {

public:

    Gene(Service *service, Host *host, unsigned value);

    Service *service;
    Host *host;
    unsigned value;
};


#endif //FOG_GENE_H

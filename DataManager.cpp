//
// Created by Mohamad Nour Charaf on 8/7/20.
//

#include "DataManager.h"

std::vector<Service *> * DataManager::getScenario1Services() {
    std::vector<Service *> *allServices = getAllServices();
    std::vector<Service *> *scenario1Services = new std::vector<Service *>();
    scenario1Services->push_back(allServices->at(0));
    scenario1Services->push_back(allServices->at(1));
    scenario1Services->push_back(allServices->at(2));

    return scenario1Services;
}

std::vector<Host *> * DataManager::getScenario1Hosts() {

    std::vector<Host *> *scenario1Hosts = getAllHosts();

    return scenario1Hosts;
}

std::vector<Service *> * DataManager::getScenario2Services() {

    std::vector<Service *> *scenario2Services = getAllServices();

    return scenario2Services;
}

std::vector<Host *> * DataManager::getScenario2Hosts() {

    std::vector<Host *> *allHosts = getAllHosts();

    std::vector<Host *> *scenario2Hosts = new std::vector<Host *>();
    scenario2Hosts->push_back(allHosts->at(0));
    scenario2Hosts->push_back(allHosts->at(1));
    scenario2Hosts->push_back(allHosts->at(2));
    scenario2Hosts->push_back(allHosts->at(6));

    return scenario2Hosts;
}

std::vector<Service *> * DataManager::getAllServices(){

    Service *S1 = new Service("S1",0.12,0.2,0.2,0);
    Service *S2 = new Service("S2",0.24,0.23,0.1,1);
    Service *S3 = new Service("S3",0.18,0.2,0.32,0);
    Service *S4 = new Service("S4",0.25,0.42,0.2,1);
    Service *S5 = new Service("S5",0.31,0.15,0.24,1);
    Service *S6 = new Service("S6",0.375,0.175,0.08,0);

    std::vector<Service *> S = {S1,S2,S3,S4,S5,S6};

    std::vector<Service *> *services = new std::vector<Service *>();

    services->insert(services->end(), S.begin(), S.end() );

    return services;
}

std::vector<Host *> * DataManager::getAllHosts(){

    Host *H1 = new Host("H1",0.5,0.24,0.4,2000,500);
    Host *H2 = new Host("H2",0.25,0.4,0.4,500,50);
    Host *H3 = new Host("H3",0.25,0.26,0.2,300,40);
    Host *H4 = new Host("H4",0.8,0.24,0.38,1000,25);
    Host *H5 = new Host("H5",0.2,0.76,0.62,70,50);
    Host *H6 = new Host("H6",0.6,0.6,0.5,60,1000);
    Host *H7 = new Host("H7",1,1,1,65,20);

    std::vector<Host *> H = {H1,H2,H3,H4,H5,H6,H7};

    std::vector<Host *> *hosts = new std::vector<Host *>();

    hosts->insert(hosts->end(), H.begin(), H.end() );

    return hosts;
}